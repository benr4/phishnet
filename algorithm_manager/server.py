from server import app
from pyvirtualdisplay import Display

# start virtual display
display = Display(visible=0, size=(1280, 720))
display.start()
# start rest API
app.run(host='0.0.0.0', port=80, debug=False)

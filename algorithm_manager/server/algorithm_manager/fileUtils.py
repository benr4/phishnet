import os
from shutil import rmtree

DOMAINS_FILE = "domains"
LOCK_FILE = "lock"


def get_filename_from_url(url, extension="") -> str:
    ret = str(url).replace("/", ".")
    if extension != "":
        ret += "."+extension
    return ret


def create_directory(dirname):
    try:
        os.mkdir(str(dirname))
    except:
        pass


def delete_directory(dirname):
    rmtree(str(dirname))


def write_domains_to_file(dirname, domains):
    with open(os.path.join(dirname, DOMAINS_FILE),"w") as f:
        for domain in domains:
            f.write(str(domain))
            f.write("\n")


def create_lock_file(dirname):
    open(os.path.join(dirname, LOCK_FILE), "w").close()


def check_if_lock_exists(dirname):
    return os.path.exists(os.path.join(dirname, LOCK_FILE))


def read_domains_from_file(dirname):
    with open(os.path.join(dirname, DOMAINS_FILE)) as f:
        lines = f.readlines()
    return lines


def delete_lock_file(dirname):
    os.remove(os.path.join(dirname, LOCK_FILE))

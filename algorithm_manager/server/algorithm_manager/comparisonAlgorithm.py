import os
from .screenshotUtil import ScreenshotUtility
from .imageComparisonUtil import get_similarity
from threading import Thread, Lock
import queue
import time
import cv2
from .textAndHTMLComparisonUtil import HtmlAndTextComparator
import logging
from .fileUtils import get_filename_from_url, delete_directory,create_directory, write_domains_to_file, \
    create_lock_file, check_if_lock_exists

logging.basicConfig(level=logging.INFO)


class ComparisonAlgorithm:

    def __init__(self, domain_name, suspects, identifier):
        self.domain_name = domain_name
        # list of suspect domains (string)
        self.suspects = suspects
        self.img_scores = {}
        self.txt_scores = {}
        self.id = identifier
        self.num_threads = 5

    def compare_parallel(self, sc_queue):
        logging.info("Starting comparison algorithm for " + str(self.domain_name) + " report ID " + str(self.id))

        if len(self.suspects) == 0:
            return

        # jobs queues
        img_jobs = queue.Queue()
        txt_jobs = queue.Queue()
        for url, redirected in self.suspects:
            img_jobs.put((url, redirected))
            txt_jobs.put((url, redirected))

        img_workers = []
        txt_workers = []

        # compare by text
        for _ in range(self.num_threads):
            worker = TextComparatorThread(self.domain_name, txt_jobs)
            worker.setDaemon(True)
            worker.start()
            txt_workers.append(worker)

        # prepare the screenshots
        self.prepare_screenshots(sc_queue)

        # compare by image
        for _ in range(self.num_threads):
            worker = ImageComparatorThread(self.domain_name, img_jobs, self.id)
            worker.setDaemon(True)
            worker.start()
            img_workers.append(worker)

        for i in range(self.num_threads):
            img_workers[i].join()
            txt_workers[i].join()

        delete_directory(get_filename_from_url(self.domain_name)+"_"+str(self.id))

        for i in range(self.num_threads):
            self.img_scores.update(img_workers[i].scores)
            self.txt_scores.update(txt_workers[i].scores)

        logging.info("Finished comparison algorithm for " + str(self.domain_name) + " report ID " + str(self.id))


    def prepare_screenshots(self, sc_queue):
        dirname = get_filename_from_url(self.domain_name)+"_"+str(self.id)

        # create the report directory
        create_directory(dirname)
        temp_suspects = [suspect[1] for suspect in self.suspects.copy()]
        temp_suspects.insert(0, self.domain_name)
        # write all domains to domains file
        write_domains_to_file(dirname, temp_suspects)

        # create the domains ready file
        create_lock_file(dirname)

        # enqueue the job in the screenshots queue
        sc_queue.put(dirname)

        # wait for screenshots process to finish
        while check_if_lock_exists(dirname):
            time.sleep(1)

    def get_scores(self):
        return self.img_scores, self.txt_scores


class ImageComparatorThread(Thread):

    def __init__(self, src_url, suspects_q, identifier):
        Thread.__init__(self)
        self.src_url = src_url
        self.suspects_q = suspects_q
        self.id = identifier
        self.kill_received = False
        self.scores = {}

    def run(self):
        logging.debug('ImageComparatorThread started')
        dirname = get_filename_from_url(self.src_url) + "_" + str(self.id)
        try:
            origin_img = cv2.imread(os.path.join(dirname,
                                                 get_filename_from_url(self.src_url, extension="png")), 0)
        except:
            logging.debug('Could not read original URL image')
            return

        while not self.kill_received:
            try:
                domain, redirected = self.suspects_q.get(block=False)
            except queue.Empty:
                self.kill_received = True
                return

            if domain:
                try:
                    img = cv2.imread(os.path.join(dirname,
                                                  get_filename_from_url(redirected, extension="png")), 0)
                    self.scores[domain] = get_similarity(origin_img, img) * 100.0
                    logging.debug('image_comparator: %s score %s', str(domain), str(self.scores[domain]))
                except:
                    logging.debug('could not read image %s', str(domain)+".png")
                    self.scores[domain] = -1


class TextComparatorThread(Thread):

    def __init__(self, src_url, suspects_q):
        Thread.__init__(self)
        self.src_url = src_url
        self.suspects_q = suspects_q
        self.kill_received = False
        self.scores = {}

    def run(self):
        while not self.kill_received:
            try:
                domain, redirected = self.suspects_q.get(block=False)
            except queue.Empty:
                self.kill_received = True
                return

            if domain:
                html_comparator = HtmlAndTextComparator("http://" + self.src_url)
                self.scores[domain] = html_comparator.compare("http://"+redirected)
                logging.debug('text_comparator: %s score %s', str(domain), str(self.scores[domain]))





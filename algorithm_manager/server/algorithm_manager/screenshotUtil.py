import os
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time
import logging
from threading import Thread, Lock
import queue
from .fileUtils import read_domains_from_file, get_filename_from_url, delete_lock_file


logging.basicConfig(level=logging.INFO)


lock = Lock()
jobs_counter = 0


class ScreenshotUtility:

    def __init__(self, jobs_mpq):
        self.jobs_mpq = jobs_mpq
        self.num_threads = 4

    def start_workers(self):
        global jobs_counter
        workers_q = queue.Queue()
        workers = []

        for i in range(self.num_threads):
            worker = ScreenshotThread(workers_q)
            worker.setDaemon(True)
            worker.start()
            workers.append(worker)
            time.sleep(10.0)

        while True:
            try:
                job = self.jobs_mpq.get()
            except:
                time.sleep(5)
                continue

            logging.info('screenshots job %s started', str(job))

            # read all urls from urls file
            domains_list = read_domains_from_file(get_filename_from_url(str(job)))

            jobs_counter = len(domains_list)

            # fill the jobs queue of the working threads
            for domain in domains_list:
                workers_q.put((str(domain).replace("\n", ""), get_filename_from_url(str(job).replace("\n", ""))))

            while True:#not workers_q.empty():
                lock.acquire()
                try:
                    if jobs_counter == 0:
                        break
                finally:
                    lock.release()
                time.sleep(5.0)

            # change the status file
            delete_lock_file(get_filename_from_url(job))
            logging.info('screenshots job %s finished', str(job))


class ScreenshotThread(Thread):

    def __init__(self, jobs):
        Thread.__init__(self)
        self.jobs = jobs

    def run(self):
        global jobs_counter
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--new-instance")
        profile = webdriver.FirefoxProfile()
        try:
            browser = webdriver.Firefox(options=options, firefox_profile=profile, executable_path='/usr/bin/geckodriver')
        except Exception as e:
            logging.debug(str(e))
            try:
                browser = webdriver.Firefox(options=options, firefox_profile=profile,executable_path='/opt/geckodriver-v0.29.1')
            except Exception as ex:
                logging.debug(str(ex))
                try:
                    browser = webdriver.Firefox(options=options, firefox_profile=profile)
                except Exception as e2:
                    logging.debug(str(e2))

        while True:
            try:
               url, dirname = self.jobs.get()
            except:
                time.sleep(5)
                continue

            logging.debug('got job %s dirname %s',  get_filename_from_url(url, extension="png"), str(dirname))

            try:
                browser.get("http://" + str(url))
                time.sleep(1.5)
                if not browser.save_screenshot(os.path.join(dirname, get_filename_from_url(url, extension="png"))):
                    logging.debug('%s could not save screenshot', get_filename_from_url(url, extension="png"))
                lock.acquire()
                jobs_counter -=1

            except:
                logging.debug('Bad url %s could not take a screenshot', str(url))
                lock.acquire()
                jobs_counter -= 1
                continue
            finally:
                lock.release()










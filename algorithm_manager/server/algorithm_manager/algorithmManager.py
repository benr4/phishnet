from .scanAlgorithm import ScanAlgorithm
from .comparisonAlgorithm import ComparisonAlgorithm
import requests
import logging
from enum import Enum


logging.basicConfig(level=logging.INFO)


class AlgorithmManager:

    class RiskLevel(Enum):
        LOW = 0
        MEDIUM = 1
        HIGH = 2

    HIGH_SCORE_THRESHOLD = 50
    MEDIUM_SCORE_THRESHOLD = 30

    def __init__(self, domain, report_id, risk_level, screenshots_q):
        self.domain = domain.replace("http://", "")
        self.report_id = int(report_id)
        self.risk_level = AlgorithmManager.RiskLevel(int(risk_level))
        self.screenshots_q = screenshots_q

    def createReport(self):
        # uses scan, image compare and text compare and returns report to the caller.
        logging.info("Starting report for " + str(self.domain) + " report ID " + str(self.report_id))
        logging.info("Starting scan for " + str(self.domain) + " report ID " + str(self.report_id))
        scan_domains = self.scan()
        logging.info("Finished scan for " + str(self.domain) + " report ID " + str(self.report_id))
        domains = list(filter(lambda x: str(x['fuzzer']) != str('original*'), scan_domains))
        self.prepare_redirects(domains)
        domains_urls = self.domains_to_urls_list(domains)
        img_scores,txt_scores = self.compare(domains_urls)
        scores, match_types = self.merge_scores(img_scores, txt_scores)
        suspects = self.prepare_suspects(scores, domains, match_types)
        self.send_report(suspects)
        logging.info("Finished report for " + str(self.domain) + " report ID " + str(self.report_id))

    def prepare_redirects(self, domains):
        for domain in domains:
            try:
                req = requests.get("http://"+str(domain['domain-name']), timeout=5)
                domain['last-redirect'] = str(req.url).replace("http://", "")
                domain['last-redirect'] = str(domain['last-redirect']).replace("https://", "")
                if self.domain in domain['last-redirect']:
                    domain['is-redirect'] = True
                else:
                    domain['is-redirect'] = False
                logging.debug('%s redirected to %s', str(domain['domain-name']), str(domain['last-redirect']))
            except Exception:
                domain['last-redirect'] = domain['domain-name']
                domain['is-redirect'] = False

    def domains_to_urls_list(self, domains):
        return list([(domain['domain-name'], domain['last-redirect']) for domain in list(filter(lambda domain: domain['is-redirect'] is False, domains))])

    def scan(self):
        return ScanAlgorithm.scan(self.domain)

    def compare(self, domains_list):
        comparison_algo = ComparisonAlgorithm(self.domain, domains_list, self.report_id)
        comparison_algo.compare_parallel(self.screenshots_q)
        return comparison_algo.get_scores()

    def merge_scores(self, img_scores, txt_scores):
        scores = {}
        match_types = {}
        for key in img_scores.keys():
            img_score = img_scores.get(key)
            txt_score = txt_scores.get(key)
            if not img_score:
                img_score = -1
            if not txt_score:
                txt_score = -1

            if img_score >= 0 and txt_score >= 0:
                if img_score >= 50 and txt_score < 50 or img_score < 50 and txt_score >= 50:
                    score = max(txt_score, img_score)
                    if score == txt_score:
                        match_types[key] = "Text"
                    else:
                        match_types[key] = "Image"
                else:
                    score = (img_score + txt_score)/2
                    match_types[key] = "Image and Text"
            elif img_score >= 0 and txt_score < 0:
                score = img_score
                match_types[key] = "Image"
            elif img_score < 0 and txt_score >= 0:
                score = txt_score
                match_types[key] = "Text"
            else:
                score = 10
                match_types[key] = "Domain Name"

            if self.risk_level == AlgorithmManager.RiskLevel.HIGH:
                if score >= AlgorithmManager.HIGH_SCORE_THRESHOLD:
                    scores[key] = score
            elif self.risk_level == AlgorithmManager.RiskLevel.MEDIUM:
                if score >= AlgorithmManager.MEDIUM_SCORE_THRESHOLD:
                    scores[key] = score
            else:
                scores[key] = score

        return scores, match_types

    def prepare_suspects(self, scores, domains, match_types):
        suspects = []
        for url,score in scores.items():
            for domain in domains:
                if url == domain['domain-name']:
                    try:
                        ip = domain['dns-a']
                    except KeyError:
                        ip = ''
                    suspects.append({
                        'domain': domain['domain-name'],
                        'ip': ip,
                        'score': score,
                        'type_of_match': match_types[url]
                    })
                    break

        if self.risk_level == AlgorithmManager.RiskLevel.LOW:
            for domain in domains:
                if domain['is-redirect']:
                    try:
                        ip = domain['dns-a']
                    except KeyError:
                        ip = ''
                    suspects.append({
                        'domain': domain['domain-name'],
                        'ip': ip,
                        'score': 10,
                        'type_of_match': "Domain Name - Redirect"
                    })

        return suspects

    def send_report(self, suspects):
        try:
            requests.post(url="http://webserver:8000/api/", json={
                "domain": self.domain,
                "id": self.report_id,
                "suspects": [{"domain": suspect['domain'],
                              "ip": suspect['ip'],
                              "score": suspect['score'],
                              'type_of_match': suspect['type_of_match']} for suspect in suspects] if len(suspects) > 0 else []
            })
        except Exception as e:
            logging.warning("Failed sending report for "+str(self.domain) + " report ID " + str(self.report_id))
            logging.debug(str(e))

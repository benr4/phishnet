import ppdeep
from bs4 import BeautifulSoup
import requests


class HtmlAndTextComparator:

    def __init__(self, url):
        try:
            self.curr_hash = self.get_fuzzy_hash_from_url(url)
        except:
            self.curr_hash = None
        try:
            self.words = self.get_words_from_url(url)
        except:
            self.words = None

    def compare(self, url):
        txt_score = self.compare_text(url)
        html_score = self.compare_html(url)

        if txt_score <= 0 and html_score <= 0:
            return -1
        elif txt_score <= 0:
            return html_score
        elif html_score <= 0:
            return txt_score
        else:
            return (txt_score+html_score)*0.5

    def compare_html(self, url):
        if not self.curr_hash:
            return -1
        try:
            hash_url = self.get_fuzzy_hash_from_url(url)
        except:
            return -1
        score = ppdeep.compare(self.curr_hash, hash_url)

        return score

    def compare_text(self, url):
        if not self.words:
            return -1
        try:
            words = self.get_words_from_url(url)
        except:
            return -1
        hits =0
        for word in words:
            if word in self.words:
                hits +=1

        if len(self.words) == 0 and len(words) == 0:
            return 0
        elif len(self.words) == 0:
            return hits/len(words)*100
        elif len(words) == 0:
            return hits/len(self.words)*100
        else:
            return (hits/len(self.words)+hits/len(words))/2*100

    def get_words_from_url(self, url):
        html = requests.get(url, verify=False)
        soup = BeautifulSoup(html.content, 'html.parser')
        for script in soup(["script", "style"]):
            script.decompose()
        strips = list(soup.stripped_strings)
        words = []
        for line in strips:
            words_list = line.split()
            for word in words_list:
                words.append(word.lower())

        return set(words)


    def get_fuzzy_hash_from_url(self, url):
        req = requests.get(url, verify=False)

        my_str = BeautifulSoup(req.content, 'html.parser')

        return ppdeep.hash(str(my_str))

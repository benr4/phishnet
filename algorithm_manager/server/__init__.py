import multiprocessing
from flask import Flask
from flask_restful import Resource, Api, reqparse
from .algorithm_manager.algorithmManager import AlgorithmManager
from .algorithm_manager.screenshotUtil import ScreenshotUtility


# screenshot process entry point
def start_screenshots_process(jq):
    su = ScreenshotUtility(jq)
    su.start_workers()


# Create an instance of Flask
app = Flask(__name__)

# Create the API
api = Api(app)

# global jobs queue for the screenshot service
jobs_queue = multiprocessing.Queue()

screenshots_process = multiprocessing.Process(target=start_screenshots_process, args=(jobs_queue, ))
screenshots_process.daemon = True
screenshots_process.start()


# report entry point
def start_report(domain, report_id, risk_level, j_queue):
    manager = AlgorithmManager(domain, report_id, risk_level, j_queue)
    manager.createReport()


def schedule_report(domain, report_id, risk_level):
    process = multiprocessing.Process(target=start_report, args=(domain, report_id, risk_level, jobs_queue,))
    process.start()


class Report(Resource):

    def post(self):
        parser = reqparse.RequestParser()

        parser.add_argument('id', required=True)
        parser.add_argument('domain', required=True)
        parser.add_argument('risk_level', required=True)

        # Parse the arguments into an object
        args = parser.parse_args()
        if not args['domain'] or not args['id'] or not args['risk_level']:
            return {}, 400

        schedule_report(str(args['domain']), str(args['id']), int(args['risk_level']))

        return {}, 200


api.add_resource(Report, '/report/')

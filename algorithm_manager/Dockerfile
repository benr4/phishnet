FROM ubuntu:20.04

ENV TZ=Asia/Jerusalem
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update -y && apt-get install -y python3.9 \
        firefox \
        build-essential \
        wget \
        curl

RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
RUN apt-get -y install python3.9-distutils
RUN python3.9 get-pip.py

ARG GK_VERSION=v0.29.1
RUN wget --no-verbose -O /tmp/geckodriver.tar.gz http://github.com/mozilla/geckodriver/releases/download/$GK_VERSION/geckodriver-$GK_VERSION-linux64.tar.gz \
   && rm -rf /opt/geckodriver \
   && tar -C /opt -zxf /tmp/geckodriver.tar.gz \
   && rm /tmp/geckodriver.tar.gz \
   && mv /opt/geckodriver /opt/geckodriver-$GK_VERSION \
   && chmod 755 /opt/geckodriver-$GK_VERSION \
   && ln -fs /opt/geckodriver-$GK_VERSION /usr/bin/geckodriver

RUN apt-get install -y libopencv-dev python3-opencv
RUN apt-get install -y xvfb

COPY requirements.txt /algorithm_manager/requirements.txt

WORKDIR /algorithm_manager

RUN pip install -r requirements.txt

COPY . /algorithm_manager

CMD [ "python3.9","server.py" ]

from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import CustomUserCreationForm, CustomUserChangeForm, CustomProfileChangeForm
from django.contrib.auth.decorators import login_required
from reports.views import ScheduleScan
from users.models import CustomUser

# Create your views here.


def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            email = form.cleaned_data.get('email')
            password1 = form.clean_password2()
            domain1 = form.cleaned_data.get('domain1')
            domain2 = form.cleaned_data.get('domain2')
            domain3 = form.cleaned_data.get('domain3')
            domain4 = form.cleaned_data.get('domain4')
            domain5 = form.cleaned_data.get('domain5')
            risk_level = form.cleaned_data.get('risk_level')
            reports_interval = form.cleaned_data.get('reports_interval')
            messages.success(request, f'Your account has been created, you can login now.')
            return redirect('login')
        else:
            messages.error(request, f'ggg')
    else:
        form = CustomUserCreationForm()
    return render(request, 'users/register.html', {'form': form})


@login_required()
def profile(request):

    if request.method == 'POST':
        domains_form = CustomProfileChangeForm(request.POST, instance=request.user)
        a = request.user.reports_interval
        if domains_form.is_valid():
            domains_form.save()
            reports_interval = domains_form.cleaned_data.get('reports_interval')
            if a is not reports_interval:
                try:
                    user = CustomUser.objects.filter(id=request.user.id)
                    user.update(has_started=False)
                except Exception as e:
                    print(f'Exception in users/views. could not update user has_started stacktrace:\n {e}')

        return redirect('profile')
    else:
        domains_form = CustomProfileChangeForm(instance=request.user)

    context = {
        'domains_form': domains_form
    }

    return render(request, 'users/profile.html', context)

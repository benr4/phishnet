from django.contrib.auth.forms import UserCreationForm, UserChangeForm, forms
from .models import CustomUser
from django.forms import ModelForm


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomUser
        fields = (
            'username', 'reports_interval', 'risk_level', 'email', 'domain1', 'domain2', 'domain3', 'domain4',
            'domain5')


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = (
            'email', 'username', 'risk_level', 'reports_interval',
            'domain1', 'domain2', 'domain3', 'domain4',
            'domain5')


class CustomProfileChangeForm(ModelForm):
    email = forms.EmailField()
    username = forms.CharField(max_length=50)
    domain1 = forms.URLField(max_length=50)
    domain2 = forms.URLField(max_length=50, required=False)
    domain3 = forms.URLField(max_length=50, required=False)
    domain4 = forms.URLField(max_length=50, required=False)
    domain5 = forms.URLField(max_length=50, required=False)
    risk_level_choices = [
        ('0', 'Low'),
        ('1', 'Medium'),
        ('2', 'High'),
    ]
    report_interval_choices = [
        ('1', 'Daily'),
        ('7', 'Weekly'),
        ('30', 'Monthly'),
    ]
    reports_interval = forms.Select(choices=report_interval_choices)
    risk_level = forms.Select(choices=risk_level_choices)



    class Meta:
        model = CustomUser
        fields = ['username',  'email', 'domain1', 'domain2', 'domain3', 'domain4',
                  'domain5', 'reports_interval',
                 'risk_level']

from django.core.mail import send_mail
import os


class EmailUtil:

    @staticmethod
    def send_message(user_email, username, domain):
        domain_name = os.environ.get('DOMAIN_NAME')
        subject = f'Phishnet - {username} your report is ready!'
        message = f'Hi {username}!\n\nYour report for {domain} is ready.' \
                  f'\n To see the report please go to: {domain_name}:1337/reports/'
        sender = 'cyberproject4@gmail.com'
        send_mail(subject, message, sender, [user_email], fail_silently=False,)

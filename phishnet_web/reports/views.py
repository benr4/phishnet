from django.shortcuts import render
from .models import Reports, Suspects
import requests
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from apscheduler.schedulers.background import BackgroundScheduler
from users.models import CustomUser


@login_required()
def reportspage(request):
    context = {'reports': Reports.objects.values().filter(author_id=request.user.id).order_by('-date_requested'),
               'suspects': Suspects.objects.all()}
    return render(request, 'reports/reports-homepage.html', context)


def start_scan(request):
    print('scan started')

    if request.user.domain1:
        send_report_request_for_domain(request, request.user.domain1)
    if request.user.domain2:
        send_report_request_for_domain(request, request.user.domain2)
    if request.user.domain3:
        send_report_request_for_domain(request, request.user.domain3)
    if request.user.domain4:
        send_report_request_for_domain(request, request.user.domain4)
    if request.user.domain5:
        send_report_request_for_domain(request, request.user.domain5)
    return redirect('reports-page')


def send_report_request_for_domain(request, domain):
    external_api_url = 'http://algorithmmanager:80/report/'
    report = Reports(author_id=request.user.id, domain=domain, status='In progress', risk_level=str(request.user.risk_level))
    report.save()
    report.title = f'Report #{len(Reports.objects.filter(author_id=request.user.id).all())}'
    report.save()
    requests.post(url=external_api_url, json={'domain': domain, 'id': report.id, 'risk_level': request.user.risk_level})


class ScheduleScan:

    def __init__(self):
        self.scheduler = BackgroundScheduler()
        self.scheduler.start()
        print("scheduler started")

    def add_to_job_queue(self, request):
        try:
            self.scheduler.remove_job(f'{request.user.id}')
        except:
            print('Scheduler job id not found')

        self.scheduler.add_job(start_scan, 'interval', id=f'{request.user.id}', days=int(request.user.reports_interval), args=[request])
        try:
            user = CustomUser.objects.filter(id=request.user.id)
            user.update(has_started=True)
            user.save()
        except:
            print('Exception in save')
        print('add to job Q function called')
        return redirect('reports-page')

    def run_a_single_job(self, request):
        # if doesn't work, lose the square brackets, or add parenthesis
        self.scheduler.add_job(start_scan, 'date', args=[request])
        print("started report")
        return redirect('reports-page')


scheduler = ScheduleScan()

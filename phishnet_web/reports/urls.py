from django.urls import path
from . import views
from .views import scheduler



urlpatterns = [
    path('', views.reportspage, name='reports-page'),
    path('#', scheduler.add_to_job_queue, name='add_to_job_queue'),
    path(' ', scheduler.run_a_single_job, name='run_a_single_job'),
]


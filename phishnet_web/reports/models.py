from django.db import models
from django.utils import timezone
from users.models import CustomUser as User


# Create your models here.
class Reports(models.Model):
    risk_level_choices = [
        ('0', 'Low'),
        ('1', 'Medium'),
        ('2', 'High'),
    ]

    title = models.CharField(max_length=100)
    date_requested = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    domain = models.URLField(default='example.com')
    status = models.CharField(max_length=25, default='')
    risk_level = models.CharField(max_length=12, default=risk_level_choices[0])


    def __str__(self):
        return self.title


class Suspects(models.Model):
    report = models.ForeignKey(Reports, on_delete=models.CASCADE)
    url = models.URLField(max_length=100)
    ip = models.CharField(max_length=64)
    score = models.IntegerField()
    type_of_match = models.CharField(max_length=64, null=True)

